import java.time.LocalDateTime;
import java.util.Date;

public class Advertising {

    private String id;
    private String title;
    private User advertiser;
    private String description;
    private LocalDateTime creationDateTime;

    public Advertising(String id, String title, User advertiser, String description, LocalDateTime creationDateTime){
        this.id = id;
        this.title = title;
        this.advertiser = advertiser;
        this.description = description;
        this.creationDateTime = creationDateTime;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public User getAdvertiser() {
        return advertiser;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }
}
