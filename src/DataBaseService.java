import java.util.ArrayList;

public class DataBaseService {
    ArrayList<User> users;
    public DataBaseService(){
        users = new ArrayList<>();
    }
    public void addUser(User user){
        users.add(user);
    }

    public User getUserByUsername(String username){
        for(int i = 0; i < users.size(); i++){
            if(users.get(i).getUsername().equals(username)){
                return users.get(i);
            }
        }
        return null;
    }
}
