public class ServerParser {
    private JsonHandler jsonHandler;
    private DataBaseService dataBaseService;

    public ServerParser(JsonHandler jsonHandler, DataBaseService dataBaseService){
        this.jsonHandler = jsonHandler;
        this.dataBaseService = dataBaseService;
    }
    public String parse(String request){
        String[] commands = request.split("$");
        if(commands.equals("ADD NEW USER")){
            dataBaseService.addUser(jsonHandler.writeJsonString(commands[1]));
            return "NEW USER ADDED";
        }
        if(commands.equals("GET USER")){
            User toFind = jsonHandler.writeJsonString(commands[1]);
            for(int i = 0; i < dataBaseService.users.size(); i++){
                if(dataBaseService.users.get(i).login(toFind.getUsername(), toFind.getPassword())){
                    return "USER FOUND";
                }
            }
        }
        return "";
    }
}
