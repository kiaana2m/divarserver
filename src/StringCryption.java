public class StringCryption {
    private int key;
    public StringCryption(int key){
        this.key = key;
    }
    public String encrypt(String str){
        StringBuilder toRet = new StringBuilder();
        for(int i = 0; i < str.length(); i++){
            toRet.append((char) ((int) str.charAt(i) + key));
        }
        return toRet.toString();
    }
    public String decrypt(String str){
        StringBuilder toRet = new StringBuilder();
        for(int i = 0; i < str.length(); i++){
            toRet.append((char) ((int) str.charAt(i) - key));
        }
        return toRet.toString();
    }
}
