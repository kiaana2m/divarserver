import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class JsonHandler {
    private String userAddress;
    private String advertisingAddress;
    private DataBaseService dataBaseService;

    public JsonHandler(DataBaseService dataBaseService){
        userAddress = "/tmp/users/";
        advertisingAddress = "/tmp/advertising";
        this.dataBaseService = dataBaseService;
        File userDir = new File(userAddress);
        if(!userDir.exists()){
            userDir.mkdirs();
        }
    }
    public User readUserData(String username){
        JSONParser jsonParser = new JSONParser();
        User user = null;
        try(FileReader reader = new FileReader(userAddress + username + ".json")){
            Object object = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) object;
            user = new User(jsonObject.get("name").toString(), jsonObject.get("lastName").toString(), jsonObject.get("username").toString(), jsonObject.get("password").toString());
        } catch (IOException | ParseException e){
            e.printStackTrace();
        }
        return user;
    }

    public void writeAdvertisingData(Advertising advertising){
        JSONObject details = new JSONObject();
        details.put("title", advertising.getTitle());
        details.put("advertiser_username", advertising.getAdvertiser().getUsername());
        details.put("creation_date_time", advertising.getCreationDateTime().toString());
        details.put("description", advertising.getDescription());
        try(FileWriter file = new FileWriter(userAddress + advertising.getId() + ".json")){
            file.write(details.toJSONString());
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public Advertising readAdvertisingData(String id){
        JSONParser jsonParser = new JSONParser();
        Advertising advertising = null;
        try(FileReader reader = new FileReader(advertisingAddress + id + ".json")){
            Object object = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) object;
            User user = dataBaseService.getUserByUsername(jsonObject.get("advertiser_username").toString());
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime localDateTime = LocalDateTime.parse(jsonObject.get("creation_date_time").toString(), formatter);
            advertising = new Advertising(id, jsonObject.get("title").toString(), user, jsonObject.get("description").toString(), localDateTime);
        } catch (IOException | ParseException e){
            e.printStackTrace();
        }
        return advertising;
    }

    public void writeUserData(User user){
        JSONObject details = new JSONObject();
        details.put("name", user.getName());
        details.put("lastName", user.getLastName());
        details.put("username", user.getUsername());
        details.put("password", user.getPassword());
        try(FileWriter file = new FileWriter(userAddress + user.getUsername() + ".json")) {
            file.write(details.toJSONString());
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public String getJsonString(User user){
        JSONObject employeeDetails = new JSONObject();
        employeeDetails.put("name", user.getName());
        employeeDetails.put("lastName", user.getLastName());
        employeeDetails.put("username", user.getUsername());
        employeeDetails.put("password", user.getPassword());
        return employeeDetails.toJSONString();
    }

    public User writeJsonString(String data){
        try(FileWriter file = new FileWriter(userAddress + "swap" + ".json")) {
            file.write(data);
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return readUserData("swap");
    }
    public ArrayList<User> getUsers(){
        ArrayList<User> users = new ArrayList<>();
        File folder = new File(userAddress);
        File[] listOfFiles = folder.listFiles();
        for(int i = 0; i < listOfFiles.length; i++){
            String[] names = listOfFiles[i].getName().split("\\.");
            if(names.length > 0){
                users.add(readUserData(names[0]));
            }
        }
        return users;
    }
}
