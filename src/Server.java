import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.*;

public class Server {
    private ServerSocket serverSocket = null;
    private int port;
    private ServerParser serverParser;

    public Server(int port, ServerParser serverParser){
        this.port = port;
        this.serverParser = serverParser;
    }
    public void startServer(){
        try {
            serverSocket = new ServerSocket(port);
            serverSocket.setReuseAddress(true);
            while (true){
                Socket client = serverSocket.accept();
                System.out.println("New client connected " + client.getInetAddress().getHostAddress());
                ClientHandler clientSock = new ClientHandler(client, serverParser);
                new Thread(clientSock).start();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        finally {
            if (serverSocket != null) {
                try {
                    serverSocket.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
